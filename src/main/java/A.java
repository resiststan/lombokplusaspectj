import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class A {
    private String string;
    private Integer integer;

    @CatchNPE
    public String getStrSub() {
        System.out.println("getStrSub");

        if (true) {
            throw new NullPointerException();
        }

        return string.substring(2);
    }
}